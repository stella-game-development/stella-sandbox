#include <stella.h>

#include <stdio.h>

class hello_world_application : public stla::application
{
public:

    hello_world_application() : stla::application({ 750, 750, "Hello World", true, false })
    {
        m_command_buffer.create();
    }

    ~hello_world_application()
    {
        m_command_buffer.destroy();
    }

    virtual void on_update(flecs::world &world, float dt) override 
    {
    }

    virtual void on_render(flecs::world &world, float dt) override
    {
        stla::window &window = stla::application::get_window();

        stla::gfx::region_2d render_region(0, 0, window.get_width(), window.get_height());

        m_command_buffer.begin_rendering(render_region);
        m_command_buffer.set_state(stla::gfx::draw_state(true, true, true));
        m_command_buffer.clear(stla::gfx::color((glm::sin(stla::utils::get_time() / 1000.0f) + 1.0f) * 0.25f, 0.15f, 0.15f, 1.0f), render_region);
        m_command_buffer.end_rendering();

        m_command_buffer.execute();
    }

    virtual void on_event(flecs::world &world, const stla::event::quit&) override { world.quit(); }

private:
    stla::gfx::command_buffer m_command_buffer;
};

int main() 
{
    stla::application *p_app = new hello_world_application();
    p_app->run();
    delete p_app;
}