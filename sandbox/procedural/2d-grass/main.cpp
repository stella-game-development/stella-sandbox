#include <stella.h>

#include <PerlinNoise.hpp>

#include <stdio.h>

#include <vector>
#include <functional>

struct procedural_grass_blade 
{
    STLA_PACK(struct vertex
    {
        glm::vec3 position;
        float pct_of_height;
        glm::vec3 color;
        float wind_intensity;
        float bend;
    });
    
    static inline const float s_model_height = 1.5f;

    static inline const std::vector<vertex> s_model_vertices = {
        // Section 0
        { { -0.25f, 0.5f, 0.0f }, 0.0f, stla::color::hex_to_rgb(0x5D7061), 0.0f, 0.0f },
        { { -0.25f, 0.0f, 0.0f }, 0.0f, stla::color::hex_to_rgb(0x5D7061), 0.0f, 0.0f },
        { {  0.25f, 0.0f, 0.0f }, 0.0f, stla::color::hex_to_rgb(0x5D7061), 0.0f, 0.0f },

        { {  0.25f, 0.5f, 0.0f }, 0.0f, stla::color::hex_to_rgb(0x5D7061), 0.0f, 0.0f },
        { { -0.25f, 0.5f, 0.0f }, 0.0f, stla::color::hex_to_rgb(0x5D7061), 0.0f, 0.0f },
        { {  0.25f, 0.0f, 0.0f }, 0.0f, stla::color::hex_to_rgb(0x5D7061), 0.0f, 0.0f },

        // Section 1
        { { -0.20f, 1.0f, 0.0f }, 0.0f, stla::color::hex_to_rgb(0x5D7061), 0.0f, 0.0f },
        { { -0.25f, 0.5f, 0.0f }, 0.0f, stla::color::hex_to_rgb(0x5D7061), 0.0f, 0.0f },
        { {  0.25f, 0.5f, 0.0f }, 0.0f, stla::color::hex_to_rgb(0x5D7061), 0.0f, 0.0f },

        { {  0.20f, 1.0f, 0.0f }, 0.0f, stla::color::hex_to_rgb(0x5D7061), 0.0f, 0.0f },
        { { -0.20f, 1.0f, 0.0f }, 0.0f, stla::color::hex_to_rgb(0x5D7061), 0.0f, 0.0f },
        { {  0.25f, 0.5f, 0.0f }, 0.0f, stla::color::hex_to_rgb(0x5D7061), 0.0f, 0.0f },
    
        // Section 2
        { {  0.00f, 1.5f, 0.0f }, 0.0f, stla::color::hex_to_rgb(0x5D7061), 0.0f, 0.0f },
        { { -0.20f, 1.0f, 0.0f }, 0.0f, stla::color::hex_to_rgb(0x5D7061), 0.0f, 0.0f },
        { {  0.20f, 1.0f, 0.0f }, 0.0f, stla::color::hex_to_rgb(0x5D7061), 0.0f, 0.0f },
    };

    static inline const std::vector<stla::gfx::vertex_attribute_descriptor> s_vertex_layout = {
        { stla::gfx::vertex_attribute_type::f, 3 },
        { stla::gfx::vertex_attribute_type::f, 1 },
        { stla::gfx::vertex_attribute_type::f, 3 },
        { stla::gfx::vertex_attribute_type::f, 1 },
        { stla::gfx::vertex_attribute_type::f, 1 },
    };

    struct settings
    {
        glm::vec2 scale_min;
        glm::vec2 scale_max;
        glm::vec2 x_range;
        float z;
    };

    static void push_blade(const glm::vec3 &translation, const glm::vec3 &scale, float bend, std::vector<vertex> &vertices)
    {
        size_t vertices_size = vertices.size();

        const siv::PerlinNoise perlin{ 0xABCD };
        float time = static_cast<float>(stla::utils::get_time());
        float wind_intensity = perlin.noise2D(translation.x, time / 1500.0f);

        for (vertex v : s_model_vertices) {

            v.pct_of_height = v.position.y / s_model_height;
            v.wind_intensity = wind_intensity;
            v.bend = bend;

            v.position.x *= 0.5f;

            v.position *= scale;
            v.position += translation;

            vertices.push_back(v);
        }
    
        for (size_t i = vertices_size; i < vertices.size(); i++) {
            vertices[i].position += translation;
        }
    }

    static void generate_blades(const settings &settings, uint32_t num, flecs::world &world, std::function<float(const procedural_grass_blade::settings &, const glm::vec2 &)> query_terrain_height)
    {
        const siv::PerlinNoise perlin{ 0xDEAD };
        
        for (uint32_t i = 0; i < num; i++) {
            glm::vec3 position = glm::vec3(
                stla::utils::random_range(settings.x_range.x, settings.x_range.y), 0.0f, settings.z);

            position.y = query_terrain_height(settings, glm::vec2(position.x, position.z));

            world.entity()
                .set<stla::ecs_core::position>({ position })
                .set<stla::ecs_core::scale>({ 
                    glm::vec3(stla::utils::random_range(settings.scale_min.x, settings.scale_max.x), 
                    stla::utils::random_range(settings.scale_min.y, settings.scale_max.y), 
                    1.0f) })
                .add<procedural_grass_blade>();
        }
    }
};

class procedural_grass_2d : public stla::application
{
public:

    static inline const uint32_t MAX_GRASS_BLADES = 10000;
    static inline const uint32_t MAX_GRASS_BLADES_FOREGROUND = MAX_GRASS_BLADES * 0.25f;
    static inline const uint32_t MAX_GRASS_BLADES_BACKGROUND = MAX_GRASS_BLADES * 0.75f;

    procedural_grass_2d() : stla::application({ 750, 750, "2D Procedural Grass", true, false })
    {
        m_command_buffer.create();
        m_vertex_shader.create(stla::gfx::shader_type::vertex, s_vertex_shader_src);
        m_fragment_shader.create(stla::gfx::shader_type::fragment, s_fragment_shader_src);
        m_uniform_buffer.create(stla::gfx::buffer_type::uniform_buffer, sizeof(shader_data));
        m_grass_vertex_buffer.create(
            stla::gfx::buffer_type::vertex_buffer, sizeof(procedural_grass_blade::vertex) * procedural_grass_blade::s_model_vertices.size() * MAX_GRASS_BLADES);
    }

    ~procedural_grass_2d()
    {
        m_command_buffer.destroy();
        m_vertex_shader.destroy();
        m_fragment_shader.destroy();
        m_grass_vertex_buffer.destroy();
    }

    virtual void on_startup(flecs::world &world) override
    {
        stla::window &window = stla::application::get_window();
        
        m_camera = stla::perspective_camera(60.0f, window.get_aspect_ratio(), 0.001f, 1000.0f, glm::vec3(0.0f, 25.0f, 50.0f), glm::normalize(glm::vec3(0.0f, -0.05f, -1.0f)));
        
        const siv::PerlinNoise perlin{ 0xDEAD };
        auto query_terrain_height   = [&](const procedural_grass_blade::settings &settings, const glm::vec2 &xz) { return perlin.noise2D_01(xz.x * 0.25f, xz.y); };
        auto query_terrain_height_0 = [&](const procedural_grass_blade::settings &settings, const glm::vec2 &xz) { return perlin.noise2D_01(xz.x * 0.50f, xz.y); };
        auto query_terrain_height_1 = [&](const procedural_grass_blade::settings &settings, const glm::vec2 &xz) { return perlin.noise2D_01(xz.x * 1.00f, xz.y); };

        procedural_grass_blade::generate_blades({ { 0.25f, 0.05f }, { 0.75f, 1.00f }, { -12.0f,  15.0f }, -0.5f }, 750, world, query_terrain_height);
        procedural_grass_blade::generate_blades({ { 0.50f, 0.50f }, { 1.00f, 2.00f }, { -8.00f, -5.50f },  0.0f }, 50, world, query_terrain_height);
        procedural_grass_blade::generate_blades({ { 0.50f, 1.00f }, { 1.00f, 3.00f }, { -10.0f, -8.00f },  0.0f }, 50, world, query_terrain_height);
        procedural_grass_blade::generate_blades({ { 0.50f, 0.75f }, { 1.00f, 2.50f }, { -12.0f, -10.0f },  0.0f }, 50, world, query_terrain_height);
        procedural_grass_blade::generate_blades({ { 0.50f, 0.50f }, { 1.00f, 2.00f }, { -15.0f, -12.0f },  0.0f }, 100, world, query_terrain_height);

        procedural_grass_blade::generate_blades({ { 0.25f, 0.05f }, { 0.75f, 1.00f }, { -12.0f,  15.0f }, 0.5f }, 750, world, query_terrain_height_0);
        procedural_grass_blade::generate_blades({ { 0.25f, 0.05f }, { 0.75f, 1.50f }, { -15.0f,  15.0f }, 2.0f }, 500, world, query_terrain_height_0);
        procedural_grass_blade::generate_blades({ { 0.25f, 0.01f }, { 0.75f, 1.25f }, { -20.0f,  20.0f }, 4.0f }, 750, world, query_terrain_height);

        auto query_terrain_height_2 = [&](const procedural_grass_blade::settings &settings, const glm::vec2 &xz) { 
            float t = (xz.x - (-2.0f)) / (20.0f - (-2.0f));
            t = 1.0f - glm::pow(1.0f - t, 5);

            float noise = perlin.noise2D_01(xz.x * 0.15f, xz.y);
            
            return ((t * noise) * 6.0f) - 1.0f;
        };
        
        auto query_terrain_height_3 = [&](const procedural_grass_blade::settings &settings, const glm::vec2 &xz) { 
            float t = (xz.x - (-10.0f)) / (20.0f - (-10.0f));
            t = 1.0f - glm::pow(1.0f - t, 5);

            float noise = perlin.noise2D_01(xz.x * 0.05f, xz.y);
            
            return ((t * noise) * 6.0f) - 1.0f;
        };

        auto query_terrain_height_4 = [&](const procedural_grass_blade::settings& settings, const glm::vec2& xz) {
            float t = (xz.x - (5.0f)) / (15.0f - (5.0f));
            t = 1.0f - glm::pow(1.0f - t, 5);

            float noise = perlin.noise2D_01(xz.x * 0.15f, xz.y);

            return ((t * noise) * 3.0f) - 0.0f;
        };

        procedural_grass_blade::generate_blades({ { 0.05f, 0.5f }, { 1.0f, 1.50f }, { -4.0f, 17.0f }, -5.0f }, 250, world, query_terrain_height_2);
        procedural_grass_blade::generate_blades({ { 0.05f, 1.5f }, { 1.0f, 3.50f }, { -5.0f, 2.5f }, -4.5f }, 50, world, query_terrain_height_2);
        procedural_grass_blade::generate_blades({ { 0.05f, 0.1f }, { 1.0f, 1.25f }, { -10.0f, 20.0f }, -4.0f }, 550, world, query_terrain_height_3);
        procedural_grass_blade::generate_blades({ { 0.05f, 0.5f }, { 1.0f, 1.50f }, {  5.0f, 20.0f }, -2.0f }, 150, world, query_terrain_height_4);
        procedural_grass_blade::generate_blades({ { 0.05f, 0.5f }, { 1.0f, 1.50f }, { -3.0f, 20.0f }, -2.0f }, 150, world, query_terrain_height_2);
    }

    virtual void on_update(flecs::world &world, float dt) override 
    {

    }

    virtual void on_render(flecs::world &world, float dt) override
    {
        stla::window &window = stla::application::get_window();

        stla::gfx::region_2d render_region(0, 0, window.get_width(), window.get_height());
        const stla::gfx::draw_state draw_state(true, true, true);
        const stla::gfx::color clear_color(0.0f, 0.0f, 0.0f, 1.0f);

        m_shader_data.view_projection = m_camera.get_view_projection();
        m_shader_data.time_ms = static_cast<float>(stla::utils::get_time());

        m_command_buffer.begin_rendering(render_region);
        m_command_buffer.set_state(draw_state);
        m_command_buffer.clear(clear_color, render_region);
        m_command_buffer.bind_program(m_vertex_shader, m_fragment_shader);
        m_command_buffer.copy(sizeof(shader_data), 0, m_uniform_buffer, &m_shader_data);
        m_command_buffer.bind_uniform_buffer(m_uniform_buffer, 0);

        std::vector<procedural_grass_blade::vertex> grass_vertices;

        world.each([&](
            flecs::entity e, 
            const procedural_grass_blade &, 
            const stla::ecs_core::position &position, 
            const stla::ecs_core::scale &scale) {
            procedural_grass_blade::push_blade(position.p, scale.s, scale.s.y / 5.0f, grass_vertices);
        });

        m_command_buffer.copy(grass_vertices.size() * sizeof(procedural_grass_blade::vertex), 0, m_grass_vertex_buffer, grass_vertices.data());
        m_command_buffer.bind_buffer(m_grass_vertex_buffer);
        m_command_buffer.set_vertex_layout(procedural_grass_blade::s_vertex_layout);
        m_command_buffer.draw(stla::gfx::primitive_type::triangle, grass_vertices.size());

        m_command_buffer.end_rendering();
        m_command_buffer.execute();
    }

    virtual void on_event(flecs::world &world, const stla::event::quit &) override { world.quit(); }

private:

    STLA_PACK(struct shader_data
    {
        glm::mat4 view_projection;
        float time_ms;
    });

    stla::gfx::command_buffer m_command_buffer;
    stla::gfx::shader m_vertex_shader;
    stla::gfx::shader m_fragment_shader;
    stla::gfx::buffer m_uniform_buffer;
    stla::gfx::buffer m_grass_vertex_buffer;
    shader_data m_shader_data;
    stla::perspective_camera m_camera;

    static inline const char *s_vertex_shader_src = R"(
        #version 460 core
        #extension GL_ARB_bindless_texture : require
        
        layout (location = 0) in vec3 a_position;
        layout (location = 1) in float a_pct_of_height;
        layout (location = 2) in vec3 a_color;
        layout (location = 3) in float a_wind_intensity;
        layout (location = 4) in float a_bend;
        
        layout (std140, binding = 0) uniform shader_data
        {
            mat4 u_view_projection;
            float u_time_ms;
        };

        out vec3 v_color;
        out float v_pct_of_height;
        out flat float v_distance;

        #define M_PI 3.1415926535897932384626433832795
        
        float ease_in_sine(float x)
        {
            return 1.0 - cos((x * M_PI) / 2.0);
        } 

        float ease_in_quad(float x)
        {
            return x * x;
        }

        float ease_in_cubic(float x)
        {
            return x * x * x;
        }
        
        void main()
        {
            v_pct_of_height = a_pct_of_height;// a_position.y / 8.0;
            v_color = a_color; 
            v_distance = abs(a_position.z);

            float x = u_time_ms / 250.0;
         
            float amplitude      = 1.5 * ease_in_sine(a_wind_intensity);
            float period         = 2.0 * M_PI;
            float phase_shift    = (M_PI * (1.0 - a_pct_of_height));// + random_number_1;// + pow(random_number_1, 3));
            float vertical_shift = 0.0;

            float A = amplitude;
            float B = (2.0 * M_PI) / period;
            float C = phase_shift;
            float D = vertical_shift;

            float trig_value = 1.0f;// A * sin(B * (x + C)) + D;

            vec3 position = a_position;
            position.x += trig_value * ease_in_cubic(a_pct_of_height) * a_bend * a_wind_intensity;
            //position.x += ease_in_cubic(a_pct_of_height) * a_bend * a_wind_intensity;
         
            gl_Position = u_view_projection * vec4(position, 1.0);
        }
    )";

    static inline const char *s_fragment_shader_src = R"(
        #version 460 core
        #extension GL_ARB_bindless_texture : require

        layout (std140, binding = 0) uniform shader_data
        {
            mat4 u_view_projection;
            float u_time_ms;
        };
        
        out vec4 f_color;
        
        in vec3 v_color;
        in float v_pct_of_height;
        in flat float v_distance;

        #define M_PI 3.1415926535897932384626433832795

        float ease_out_quart(float x) 
        {
            return 1.0 - pow(1.0 - x, 4);
        }

        float ease_in_sine(float x)
        {
            return 1.0 - cos((x * M_PI) / 2.0);
        }

        float ease_in_out_sine(float x)
        {
            return -(cos(M_PI * x) - 1.0) / 2.0;
        }

        float ease_out_sine(float x)
        {
            return sin((x * M_PI) / 2.0);
        }

        void main()
        {
            f_color = vec4(v_color.xyz * 0.5 * v_pct_of_height * (1.0f - (v_distance / 20.0)), 1.0);
        }
    )";
};

int main() 
{
    stla::application *p_app = new procedural_grass_2d();
    p_app->run();
    delete p_app;
}