#include <stella.h>
#include <glad/glad.h>

#include <stdio.h>

#include <iostream>
#include <string>

static const uint32_t IMAGE_WIDTH = 800;
static const uint32_t IMAGE_HEIGHT = 600;

class hello_world_voxel : public stla::application
{
public:

    hello_world_voxel() : stla::application({ 750, 750, "Hello World Compute", true, false })
    {
        m_command_buffer.create();

        stla::gfx::image_info info = {
            stla::gfx::image_type::texture,
            stla::gfx::pixel_format::R8G8B8A8F,
            { IMAGE_WIDTH, IMAGE_HEIGHT },
            stla::gfx::sampler_wrap_type::clamp_to_edge,
            stla::gfx::sampler_filter_type::nearest
        };
        
        m_image.create(info);

        m_compute_shader.create(s_compute_shader_src);
    }

    ~hello_world_voxel()
    {
        m_command_buffer.destroy();
        m_image.destroy();
        m_compute_shader.destroy();
    }

    virtual void on_update(flecs::world &world, float dt) override 
    {
    }

    virtual void on_render(flecs::world &world, float dt) override
    {
        stla::window &window = stla::application::get_window();
        
        m_compute_shader.bind(0, m_image); 
        m_compute_shader.execute(IMAGE_WIDTH, IMAGE_HEIGHT, 1);

        window.present(m_image);
    }

    virtual void on_event(flecs::world &world, const stla::event::quit&) override { world.quit(); }

private:
    stla::gfx::command_buffer m_command_buffer;
    stla::gfx::image m_image;
    stla::gfx::compute_shader m_compute_shader;

    static inline const char * s_compute_shader_src = R"(
        #version 460 core

        layout (local_size_x = 1, local_size_y = 1, local_size_z = 1) in;

        layout (rgba32f, binding = 0) uniform image2D image_output;

        void main() {
            vec4 value = vec4(0.0, 0.0, 0.0, 1.0);
            ivec2 texelCoord = ivec2(gl_GlobalInvocationID.xy);
            
            value.x = float(texelCoord.x)/(gl_NumWorkGroups.x);
            value.y = float(texelCoord.y)/(gl_NumWorkGroups.y);
            
            imageStore(image_output, texelCoord, value);
        }
    )";
};

int main() 
{
    stla::application *p_app = new hello_world_voxel();
    p_app->run();
    delete p_app;
}