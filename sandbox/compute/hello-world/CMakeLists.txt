cmake_minimum_required(VERSION 3.8)
set(CMAKE_CXX_STANDARD 20)
project(compute_hello_world)
set_property(GLOBAL PROPERTY USE_FOLDERS ON)

add_executable(${PROJECT_NAME} ${CMAKE_CURRENT_SOURCE_DIR}/main.cpp)
target_link_libraries(${PROJECT_NAME} stella)

if (WIN32)
    add_custom_command(
        TARGET ${PROJECT_NAME} POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy
                ${SDL3_LIB_PATH}
                ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_BUILD_TYPE}/${SDL_DLL})

    add_custom_command(
        TARGET ${PROJECT_NAME} POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy
            ${FLECS_LIB_PATH}
            ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_BUILD_TYPE}/${FLECS_DLL})
endif()
