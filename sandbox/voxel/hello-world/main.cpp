#include <stella.h>
#include <glad/glad.h>

#include <stdio.h>

#include <iostream>
#include <string>

static const uint32_t IMAGE_WIDTH = 800;
static const uint32_t IMAGE_HEIGHT = 600;

namespace stla
{
namespace gfx
{ 

template<typename T>
class uniform_buffer
{
public:

    uniform_buffer(buffer_type type = buffer_type::uniform_buffer)
    {
        m_command_buffer.create();
        m_buffer.create(type, sizeof(T));
        write();
    }

    ~uniform_buffer()
    {
        m_buffer.destroy();
        m_command_buffer.destroy();
    }

    bool write()
    {
        m_command_buffer.copy(sizeof(T), 0, m_buffer, &host);
        return m_command_buffer.execute();
    }

    const buffer &get_buffer() const { return m_buffer; }

    T host;

private:
    buffer m_buffer;
    command_buffer m_command_buffer;
};

} // namespace gfx
} // namespace stla


class hello_world_compute : public stla::application
{
public:

    hello_world_compute() : 
        stla::application({ (uint32_t)(IMAGE_WIDTH * 1.75f), (uint32_t)(IMAGE_HEIGHT * 1.75f), "Hello World Compute", true, false }),
        m_volume_uniform_buffer(stla::gfx::buffer_type::shader_storage)
    {
        m_command_buffer.create();

        stla::gfx::image_info info = {
            stla::gfx::image_type::texture,
            stla::gfx::pixel_format::R8G8B8A8F,
            { IMAGE_WIDTH, IMAGE_HEIGHT },
            stla::gfx::sampler_wrap_type::clamp_to_edge,
            stla::gfx::sampler_filter_type::nearest
        };
        
        m_image.create(info);

        m_compute_shader.create(s_compute_shader_src);

        // tmp
        m_volume_uniform_buffer.host.volumes[0].aabb.min = glm::vec3(-1.0f, -1.0f, -5.0f);
        m_volume_uniform_buffer.host.volumes[0].aabb.max = glm::vec3( 1.0f,  1.0f, -4.0f);
        STLA_ASSERT_RELEASE(m_volume_uniform_buffer.write());
    }

    ~hello_world_compute()
    {
        m_command_buffer.destroy();
        m_image.destroy();
        m_compute_shader.destroy();
    }

    virtual void on_update(flecs::world &world, float dt) override 
    {
    }

    virtual void on_render(flecs::world &world, float dt) override
    {
        stla::window &window = stla::application::get_window();
        window.set_title(std::to_string(1000.0f / dt).c_str());
        
        m_camera_uniform_buffer.host.position  = glm::vec3(0.0f);
        m_camera_uniform_buffer.host.direction = glm::normalize(glm::vec3(0.0f, 0.0f, -1.0));
        m_camera_uniform_buffer.host.t_range   = glm::vec2(0.01f, 1000.0f);
        m_camera_uniform_buffer.write();

        m_scene_uniform_buffer.host.color      = glm::vec4(0.25f, 0.15f, 0.15f, 1.0f);
        m_scene_uniform_buffer.host.resolution = glm::uvec2(m_image.get_width(), m_image.get_height());
        m_scene_uniform_buffer.write();

        m_compute_shader.bind(0, m_image); 
        m_compute_shader.bind(1, m_camera_uniform_buffer.get_buffer());
        m_compute_shader.bind(2, m_scene_uniform_buffer.get_buffer());
        m_compute_shader.bind(3, m_volume_uniform_buffer.get_buffer());
        m_compute_shader.execute(IMAGE_WIDTH, IMAGE_HEIGHT, 1);

        window.present(m_image);
    }

    virtual void on_event(flecs::world &world, const stla::event::quit&) override { world.quit(); }

private:
    stla::gfx::command_buffer m_command_buffer;
    stla::gfx::image m_image;
    stla::gfx::compute_shader m_compute_shader;


    STLA_PACK(struct camera_data
    {
        glm::vec3 position;  float p0;
        glm::vec3 direction; float p1;
        glm::vec2 t_range;
    });

    STLA_PACK(struct scene_data
    {
        glm::vec4 color;
        glm::uvec2 resolution;
    });

    STLA_PACK(struct aabb
    {
        glm::vec3 min{ 0.0f }; float p0;
        glm::vec3 max{ 0.0f }; float p1;
    });

    STLA_PACK(struct voxel_volume
    {
        static inline const uint32_t VOXEL_DIMS = 32;
        aabb aabb;
        uint32_t grid[VOXEL_DIMS * VOXEL_DIMS * VOXEL_DIMS]{ 0 };
    });

    STLA_PACK(struct voxel_volume_buffer
    {
        static inline const uint32_t MAX_VOLUMES = 16;
        voxel_volume volumes[MAX_VOLUMES];
    });

    stla::gfx::uniform_buffer<camera_data> m_camera_uniform_buffer;
    stla::gfx::uniform_buffer<scene_data> m_scene_uniform_buffer;
    stla::gfx::uniform_buffer<voxel_volume_buffer> m_volume_uniform_buffer;

    static inline const char * s_compute_shader_src = R"(
        #version 460 core

        layout (local_size_x = 1, local_size_y = 1, local_size_z = 1) in;
        
        //-----------------------------------------------------------------------------
        // Structure Definitions
        //-----------------------------------------------------------------------------

        struct ray
        {
            vec3 origin;
            vec3 direction;
        };

        struct aabb
        {
            vec3 min;
            vec3 max;
        };
        
        #define VOXEL_DIMS 32
        struct voxel_volume
        {
            aabb aabb;
            uint grid[VOXEL_DIMS * VOXEL_DIMS * VOXEL_DIMS];
        };

        //-----------------------------------------------------------------------------
        // Input Variables
        //-----------------------------------------------------------------------------
        
        layout (rgba32f, binding = 0) uniform image2D image_output;        
        
        layout (std140, binding = 1) uniform camera_data
        {
            vec3 position;
            vec3 direction;
            vec2 t_range;
        } u_camera;
        
        layout (std140, binding = 2) uniform scene_data
        {
            vec4 color;
            uvec2 resolution;
        } u_scene;

        layout (std140, binding = 3) buffer voxel_volume_data
        {
            voxel_volume volumes[];
        } u_voxel_volume_buffer; 

        //-----------------------------------------------------------------------------
        // Function Definitions
        //-----------------------------------------------------------------------------

        ray new_ray(vec3 o, vec3 d);
        vec3 ray_at(ray r, float t);
        bool ray_aabb_intersects(ray r, aabb box, float t_max);

        aabb new_aabb(vec3 min, vec3 max);

        ray camera_get_ray(
            vec3 camera_position, 
            vec3 camera_direction, 
            uvec2 resolution, 
            uvec2 pixel, 
            float vfov
        ); 
        
        vec4 get_background_color(ray r) ;

        //-----------------------------------------------------------------------------
        // Main
        //-----------------------------------------------------------------------------
        
        void main() {
            vec4 value = vec4(0.0, 0.0, 0.0, 1.0);            

            ivec2 texelCoord = ivec2(gl_GlobalInvocationID.xy);
            
            value.x = float(texelCoord.x) / (gl_NumWorkGroups.x);
            value.y = float(texelCoord.y) / (gl_NumWorkGroups.y);
            
            value = u_scene.color;
            
            // -------------------------------------- //
            ray r = camera_get_ray(u_camera.position, u_camera.direction, u_scene.resolution, uvec2(gl_GlobalInvocationID.xy), 90.0);
            value = get_background_color(r);

            aabb box = u_voxel_volume_buffer.volumes[0].aabb;
            if (u_voxel_volume_buffer.volumes.length() == 0) {
                value  = vec4(1.0, 1.0, 0.0, 1.0);
            }

            // box.min = vec3(-1, -1, -5);
            // box.max = vec3(1, 1, -4);

            if (ray_aabb_intersects(r, box, 1000.0)) {
                value = vec4(1.0, 0.0, 0.0, 1.0);
            }
            
            imageStore(image_output, texelCoord, value);
        }

        //-----------------------------------------------------------------------------
        // Function Implementations
        //-----------------------------------------------------------------------------

        ray new_ray(vec3 o, vec3 d)
        {
            ray r;
            r.origin = o;
            r.direction = normalize(d);
            return r;
        }
        
        vec3 ray_at(ray r, float t)
        {
            return r.origin + (r.direction * t);
        }

        bool ray_aabb_intersects(ray r, aabb box, float t_max) 
        {
            float tmin = -t_max;
            float tmax =  t_max;

            for (int i = 0; i < 3; ++i) {
                float t1 = (box.min[i] - r.origin[i]) / r.direction[i];
                float t2 = (box.max[i] - r.origin[i]) / r.direction[i];

                tmin = max(tmin, min(t1, t2));
                tmax = min(tmax, max(t1, t2));
            }

            return tmax > max(tmin, 0.0);
        }
        
        aabb new_aabb(vec3 min, vec3 max)
        {
            aabb box;
            box.min = min;
            box.max = max;
            return box;
        } 

        ray camera_get_ray(
            vec3 camera_position, 
            vec3 camera_direction, 
            uvec2 resolution, 
            uvec2 pixel, 
            float vfov
        )
        {
            vec3 look_from = camera_position;
            vec3 look_at   = camera_direction;
            vec3 up        = vec3(0, 1, 0);
            
            uint image_width  = resolution.x;
            uint image_height = resolution.y;

            float defocus_angle = 0;
            float focus_dist    = 10.0;

            // Viewport dimensions
            float theta     = radians(vfov);
            float h         = tan(theta / 2.0);
            float vp_height = 2.0 * h * focus_dist;
            float vp_width  = vp_height * (float(image_width) / float(image_height));

            vec3 w = normalize(look_from - look_at);
            vec3 u = normalize(cross(up, w));
            vec3 v = cross(w, u);

            vec3 vp_u = vp_width * u;   // Across viewport horizontal edge
            vec3 vp_v = vp_height * -v; // Down vieport vertical edge 
            
            vec3 pixel_delta_u = vp_u / float(image_width);
            vec3 pixel_delta_v = vp_v / float(image_height);

            vec3 vp_upper_left = look_from - (focus_dist * w) - (vp_u / 2) - (vp_v / 2);
            vec3 pixel00_loc   = vp_upper_left + 0.5 * (pixel_delta_u + pixel_delta_v);

            float defocus_radius = focus_dist * tan(radians(defocus_angle / 2.0));

            vec3 defocus_disk_u = u * defocus_radius;
            vec3 defocus_disk_v = v * defocus_radius;

            vec3 pixel_sample = pixel00_loc + (pixel.x * pixel_delta_u) + (pixel.y * pixel_delta_v);

            return new_ray(look_from, pixel_sample - look_from);
        }
        
        vec4 get_background_color(ray r) 
        {
            float a = 0.5 * (r.direction.y + 1.0);
            return vec4((1.0 - a) * vec3(1.0, 1.0, 1.0) + a * vec3(0.5, 0.7, 1.0), 1.0);
        }

    )";
};

int main() 
{
    stla::application *p_app = new hello_world_compute();
    p_app->run();
    delete p_app;
}