#include <stella.h>

#include <PerlinNoise.hpp>

#include <stdio.h>

struct snow_flake
{
    glm::vec3 position;
    float scale;
    float speed;
    uint32_t seed;
};

class snow_2d : public stla::application
{
public:

    snow_2d() : stla::application({ 750, 750, "2D Snow", true, false })
    {
        m_command_buffer.create();
        m_quad_renderer.set_max_quads(1000);

        m_snow_settings.aabb = stla::math::aabb2d{
            glm::vec2(m_snow_settings.position) - (m_snow_settings.dims * 0.5f),
            glm::vec2(m_snow_settings.position) + (m_snow_settings.dims * 0.5f)
        };
    }

    ~snow_2d()
    {
        m_command_buffer.destroy();
    }

    virtual void on_startup(flecs::world &world) override
    {
        stla::window &window = stla::application::get_window();
        
        m_camera = stla::perspective_camera(45.0f, window.get_aspect_ratio(), 0.001f, 1000.0f, glm::vec3(0.0f, 0.0f, 5.0f), glm::vec3(0.0f, 0.0f, -1.0f));
        
        for (int i = 0; i < m_snow_settings.max_flakes; i++) {
            world.defer_suspend();
            flecs::entity e = world.entity().set<snow_flake>(init_flake());
            world.defer_resume();

            e.get_mut<snow_flake>()->position.y = stla::utils::random_range(m_snow_settings.aabb.min.y, m_snow_settings.aabb.max.y);
        }

        world.observer<snow_flake>()
            .event(flecs::OnRemove)
            .each([&](flecs::entity e, snow_flake &) {
                if (e.world().should_quit()) {
                    return;
                }

                flecs::world world = e.world();
                world.entity().set<snow_flake>(init_flake());
            });
    }

    virtual void on_update(flecs::world &world, float dt) override 
    {
        const float frequency    = 0.5f; // Note: needs to be an integer
        const int32_t octaves    = 8;
        const double persistence = 0.5f;

        world.each([&](flecs::entity e, snow_flake &flake) {
            if (!m_snow_settings.aabb.intersects(flake.position)) {
                e.destruct();
                return;
            }

            const siv::PerlinNoise perlin{ flake.seed };
            glm::vec3 fall_dir(perlin.noise2D(static_cast<double>(flake.position.y), static_cast<double>(flake.seed)), -1.0f, 0.0f);
            fall_dir = glm::normalize(fall_dir);
            flake.position += fall_dir * flake.speed; 
        });
    }

    virtual void on_render(flecs::world &world, float dt) override
    {
        stla::window &window = stla::application::get_window();

        stla::gfx::region_2d render_region(0, 0, window.get_width(), window.get_height());
        const stla::gfx::draw_state draw_state(true, true, true);
        const stla::gfx::color clear_color(0.40f, 0.15f, 0.15f, 1.0f);

        m_command_buffer.begin_rendering(render_region);
        m_command_buffer.set_state(draw_state);
        m_command_buffer.clear(clear_color, render_region);
        
        world.each([&](flecs::entity e, const snow_flake &flake) {
            glm::vec2 scale(flake.scale);
            
            if (!m_quad_renderer.add_quad(flake.position, scale, m_snow_settings.flake_color)) {
                m_quad_renderer.draw(m_command_buffer, m_camera.get_view_projection());
                m_quad_renderer.add_quad(flake.position, scale, m_snow_settings.flake_color);
            }
        });

        m_quad_renderer.draw(m_command_buffer, m_camera.get_view_projection());

        m_command_buffer.end_rendering();
        m_command_buffer.execute();
    }

    virtual void on_event(flecs::world &world, const stla::event::quit &) override { world.quit(); }

private:

    snow_flake init_flake()
    { 
        snow_flake flake;
        flake.position.x = stla::utils::random_range(m_snow_settings.aabb.min.x, m_snow_settings.aabb.max.x);
        flake.position.y = m_snow_settings.aabb.max.y - 0.0001f;
        flake.position.z = m_snow_settings.position.z;
        flake.scale      = stla::utils::random_range(m_snow_settings.flake_scale_range.x, m_snow_settings.flake_scale_range.y);
        flake.speed      = stla::utils::random_range(m_snow_settings.flake_speed_range.x, m_snow_settings.flake_speed_range.y);
        flake.seed       = static_cast<uint32_t>(stla::utils::random_int());
        return flake;
    }

    stla::gfx::command_buffer m_command_buffer;
    stla::quad_renderer m_quad_renderer;
    stla::perspective_camera m_camera;

    struct {
        glm::vec3 position          = glm::vec3(0.0f);
        glm::vec2 dims              = glm::vec2(5.0f, 5.0f);
        uint32_t max_flakes         = 500;
        glm::vec2 flake_scale_range = glm::vec2(0.01f, 0.04f);
        glm::vec2 flake_speed_range = glm::vec2(0.003f, 0.010f);
        glm::vec4 flake_color       = glm::vec4(glm::vec3(0.5f), 1.0f);
        stla::math::aabb2d aabb; 
    } m_snow_settings;
};

int main() 
{
    stla::application *p_app = new snow_2d();
    p_app->run();
    delete p_app;
}